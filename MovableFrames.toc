## Interface: 60000
## Title: |cFFFFFFFFMovableFrames|r
## Notes: Make Blizzard Frames Movable
## Author: Azilroka, Simpy
## Version: 1.82
## OptionalDeps: ElvUI, Tukui, Enhanced_Config
## SavedVariablesPerCharacter: MovableFramesSaved

MovableFrames.lua